import React, { Component } from "react";

class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <div className="d-flex">
                    <div style={{width: "85%"}}>
                        <button onClick={this.props.clearComplete}>Clear completed</button>
                    </div>
                    <div>
                        <label>{this.props.todosLeft} task(s) left</label>
                    </div>
                </div>
            </div>
        );
    }
}
export default Footer;