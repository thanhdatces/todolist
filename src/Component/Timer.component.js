import React, { Component } from "react";

class Timer extends Component {
    constructor(props){
        super(props);
        this.state = {time: new Date().toLocaleString()}
    }

    componentDidMount() {
        this.intervalID = setInterval(() => this.tick(),1000);
    }

    tick() {
        this.setState({time: new Date().toLocaleString()})
        if(new Date().toLocaleTimeString()==='12:00:00 AM') this.props.refresh()
    }

    componentWillUnmount() {
        clearInterval(this.intervalID);
    }

    render() {
        return <h3>{this.state.time}</h3>
    }
}
export default Timer;