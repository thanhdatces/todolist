import React, { Component } from "react";

class Header extends Component {
    
    handleSubmit = e => {
        e.preventDefault();
        const newTodo = {
            id: new Date().getTime(),
            content: this.getNewTodo.value,
            isCompleted: false,
            isSaved: false
        }
        this.props.addNewTodo(newTodo);
        this.getNewTodo.value='';
    }

    render() {
        return (
            <div className="header">
                <form onSubmit={this.handleSubmit}>
                    <input  
                            placeholder="What need to be done today?" 
                            ref={newTodo => (this.getNewTodo = newTodo)}/>
                </form>
          </div>
        );
    }
}
export default Header;