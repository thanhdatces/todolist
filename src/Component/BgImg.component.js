import React, { Component } from "react";

class BgImg extends Component {

    onChangeOpacity = e => this.props.onChangeBg(e.target.value, this.props.bgImg);
    onChangeBgImg = e => this.props.onChangeBg(this.props.opacity, e.target.value);

    render() {
        return (
            <div className="bg-control">
                <input type="range" value={this.props.opacity} min="0" max="1" onChange={this.onChangeOpacity} step="0.1"/>
                <img src="/35824.png" />
                <input placeholder="Background URL" type="text" value={this.props.bgImg} onChange={this.onChangeBgImg} />
            </div>
        )
    }
}
export default BgImg;