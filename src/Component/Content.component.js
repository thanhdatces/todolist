import React, { Component } from "react";

class Content extends Component {
    render() {
        const isNotTodo = this.props.todos.length === 0
        return (
            <div className="content">
                <table style={{marginBottom: 0}} className={isNotTodo ? "table" : "table table-hover"}>
                    <tbody>
                        {isNotTodo ? (
                            <tr>
                                <td style={{paddingLeft:"60px"}}>You haven't have any task to do</td>
                            </tr>
                        ) 
                        : this.props.todos.map(todo => (
                            <tr key={todo.id} className={todo.isCompleted ? 'completed' : ''}>
                                <td style={{width:"60px", paddingLeft: "20px"}}>{todo.isCompleted ? (<span>&radic;</span>):null}</td>
                                <td style={{width: "80%"}} className={todo.isSaved?"todoContent":""} onClick={()=>this.props.completeTodo(todo)}>{todo.content}</td>
                                <td style={{opacity: 0.5, cursor: "pointer"}}><i className={todo.isSaved?'fa fa-lock':'fas fa-save'} onClick={()=>this.props.save(todo)}></i></td>
                                <td style={{paddingRight: "60px"}}><button type="button" className="close" onClick={()=>this.props.removeTodo(todo)}>&times;</button></td>
                            </tr>
                        ))}
                    </tbody>
            </table>
          </div>
        );
    }
}
export default Content;