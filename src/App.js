import React, { Component } from "react";
import './App.scss';
import Header from './Component/Header.component';
import Content from './Component/Content.component';
import Footer from './Component/Footer.component';
import 'bootstrap/dist/css/bootstrap.css';
import Timer from "./Component/Timer.component";
import BgImg from "./Component/BgImg.component";

class App extends Component {
  constructor(props){
      super(props);
      this.state = {
        todos: localStorage.getItem("todos") ? JSON.parse(localStorage.getItem("todos")) : [],
        todosLeft: 0,
        allTodosInDate: localStorage.getItem('allTodos') || 0,
        completedTodosInDate: localStorage.getItem('completed') || 0,
        urlImage: localStorage.getItem('urlImage') || '',
        opacity: localStorage.getItem('opacity') || 1,
      };
  }

  componentDidMount() {
    this.setState({todosLeft: this.state.todos.filter(x=>!x.isCompleted).length, allTodosInDate: this.state.todos.length});
    if(localStorage.getItem('today') !== new Date().getDate().toString()) this.refresh();
    localStorage.setItem('today', new Date().getDate())
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.state.todos !== prevState.todos) {
      this.setState({todosLeft: this.state.todos.filter(x=>!x.isCompleted).length});
      localStorage.setItem("todos", JSON.stringify(this.state.todos));
    }
    if(this.state.allTodosInDate !== prevState.allTodosInDate) localStorage.setItem('allTodos', this.state.allTodosInDate);
    if(this.state.completedTodosInDate !== prevState.completedTodosInDate) localStorage.setItem('completed', this.state.completedTodosInDate);
  }

  refresh() {
    const todos = this.state.todos.filter(x=>x.isSaved).map(x=>({...x, isCompleted: false}));
    this.setState({allTodosInDate: todos.length, completedTodosInDate: 0, todayProcess: 0, todos });
    localStorage.clear();
    localStorage.setItem('todos', JSON.stringify(todos));
    localStorage.setItem('opacity', this.state.opacity);
    localStorage.setItem('urlImage', this.state.urlImage);
  }

  addNewTodo = todo => this.setState({todos: [todo, ...this.state.todos], allTodosInDate: this.state.allTodosInDate*1+1})
  completeTodo = i => this.setState({
    todos: this.state.todos.map(x=>({...x, isCompleted: x.id===i.id?!x.isCompleted:x.isCompleted})),
    completedTodosInDate: i.isCompleted ? this.state.completedTodosInDate-1 : this.state.completedTodosInDate*1+1
  })
  removeTodo = i => this.setState({todos: this.state.todos.filter(x=>x.id!==i.id), allTodosInDate: i.isCompleted ? this.state.allTodosInDate : this.state.allTodosInDate-1})
  clearComplete = () => this.setState({todos: this.state.todos.filter(x=>!x.isCompleted)})
  saveTodo = i => this.setState({todos: this.state.todos.map(x=>({...x, isSaved: x.id===i.id?!x.isSaved:x.isSaved}))});

  onChangeBg = (opacity, urlImage) => {
    this.setState({opacity, urlImage});
    localStorage.setItem('opacity', opacity);
    localStorage.setItem('urlImage', urlImage);
  }

  render() {
    const todayProcess = Math.floor(this.state.completedTodosInDate*100/this.state.allTodosInDate||0);
    return (
      <React.Fragment>
        <div className='bg-img' style={{
            backgroundImage: `url('${this.state.urlImage}')`,
            opacity: this.state.opacity
          }}/>
        <div className="All">
          <Timer refresh={this.refresh.bind(this)}/>
          <h1>Todos</h1>
          <h2>Today, I completed {todayProcess}% all tasks</h2>
          <div className="App">
              <Header addNewTodo={this.addNewTodo} />
              <Content todos={this.state.todos} completeTodo={this.completeTodo} save={this.saveTodo} removeTodo={this.removeTodo} />
              <Footer todosLeft={this.state.todosLeft} clearComplete={this.clearComplete} />
          </div>
          <BgImg opacity={this.state.opacity} bgImg={this.state.urlImage} onChangeBg={this.onChangeBg} />
        </div>
      </React.Fragment>
    );
  }
}

export default App;
