function openHerokuVersion() {
  chrome.tabs.create({
    'url': 'https://my-tasks-ntd.herokuapp.com/'
  });
}

function openNewTab() {
  chrome.tabs.create({
    'url': '/index.html'
  });
}

document.getElementById('open-new-tab').addEventListener('click', openNewTab());


